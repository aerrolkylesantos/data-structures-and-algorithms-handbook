# Introduction

In this section, you will find a list of tasks that need to be completed. These tasks are essential for the project's progress and your active participation is crucial. Each task is outlined with detailed instructions to ensure that you can efficiently complete it. Please review and address each task diligently.

### Task 1: Know yourself

1. Add your entry to the **Meet the Team** section.
2. Arrange entries in alphabetical order of the first name.
3. Add your **"pang-malupitan"** image to the **team/images** directory with a file name like **bsis_2_visaya_christian** in **.jpg** format, using camel case for the file name.
4. Follow the template for other fields; make sure to fill out all of them.
5. Add your 16Personalities link. You can test it [here](https://www.16personalities.com/).
6. Create a new branch named like **bsis-2-visaya-christian**.
   - **Branch Naming Conventions**: Use the provided example format for branch naming.
7. Commit your changes, push them to the remote repository, and create a merge request (MR).
   - **Merge Request (MR) Guidelines**: Within the MR, please include an image illustrating the expected appearance of the changes.
8. The deadline for completing this task is September 14th at 7 am.